'use strict';

const gulp = require('gulp');
const mjmlEngine = require('mjml').default;
const mjml = require('gulp-mjml');
const browsersync = require('browser-sync').create();
const panini = require('panini');
const imagemin = require("gulp-imagemin");

// Clean assets
function clean() {
    return del(["./dist"]);
}

// panini
function Panini(done){
    gulp.src('src/mjml-src/*/*.mjml')
        .pipe(panini({
            root: 'src',
            layouts: 'src/layouts/',
            partials: 'src/partials/',
            helpers: 'src/helpers/',
            data: 'src/data/'
        }))
        .pipe(gulp.dest('src/mjml-dist'));
    done();
}

// Load updated HTML templates and partials into Panini
function resetPages(done) {
    panini.refresh();
    done();
}

// Copy index.html
function copyIndex(done) {
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./dist/'));
    done();
}

// Optimize Images
function images() {
    return gulp
        .src("./src/img/**/*")
        // .pipe(newer("./_site/assets/img"))
        .pipe(
            imagemin([
                imagemin.gifsicle({ interlaced: true }),
                imagemin.jpegtran({ progressive: true }),
                imagemin.optipng({ optimizationLevel: 7 }),
                imagemin.svgo({
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                })
            ], {verbose: true})
        )
        .pipe(gulp.dest("./dist/"));
}

// mjmlHtml
function mjmlHtml(done){
    gulp.src('src/mjml-dist/*/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlBeautify(done) {
    gulp.src('src/mjml-dist/*/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: false}, {beautify: true}))
        .pipe(gulp.dest('./dist'));
    done();
}

// mjmlHtmlBeautify
function mjmlHtmlMinify(done) {
    gulp.src('src/mjml-dist/*/*.mjml')
        .pipe(mjml(mjmlEngine, {minify: true}, {beautify: false}))
        .pipe(gulp.dest('./dist'));
    done();
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./dist/"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Watch files
function watchFiles(done) {
    gulp.watch(['./src/mjml-src/*/*.mjml', './src/data/*.json'], gulp.series(links));
    gulp.watch('./src/mjml-dist/*/*.mjml', gulp.series(change));
    gulp.watch('./src/img/*', images, browserSyncReload);
    done();
}

// define complex tasks
const links = gulp.series(resetPages, Panini);
const change = gulp.series(mjmlHtml, browserSyncReload);
const build = gulp.series(resetPages, Panini, mjmlHtml, copyIndex);
const production = gulp.series(images, Panini, mjmlHtml, copyIndex);
const buildBeautify = gulp.series(Panini, mjmlHtmlBeautify, copyIndex);
const buildMinify = gulp.series(Panini, mjmlHtmlMinify, copyIndex);
const watch = gulp.parallel(watchFiles, browserSync);
const dev = gulp.series(build, watch);


// export tasks
exports.clean = clean;
exports.images = images;
exports.panini = Panini;
exports.production = production;
exports.build = build;
exports.buildBeautify = buildBeautify;
exports.buildMinify = buildMinify;
exports.watch = watch;
exports.default = dev;
